	var dist = 2;
	var twittdist = 2;
	var getpark = 'n';
	var gettoilet = 'n';
	var getdogpark = 'n';
	var getplayground = 'n';
	var getbike = 'n';
	var getskateboard = 'n';
	var getpool = 'n';
	var getgolf = 'n';
	var gettennis = 'n';
	var getcommgarden = 'n';
	var getpublicart = 'n';
	var getwifi = 'n';
	var gethelmet = 'n';
	var getpet = 'n';
	var getnursery = 'n';
	var getpicnic = 'n';
	var getbus = 'n';
	var getferry = 'n';
	var getboat = 'n';
	var getstar = 'n';
	var gettrain = 'n';
	
	// set custom icon
	function geticon(code) {
		if (code == 'park') return 'images/park2.png';
		if (code == 'dunny') return 'images/toilet2.png';
		if (code == 'dogpark') return 'images/dog2.png';		
		if (code == 'picnic') return 'images/picnic2.png';
		if (code == 'playground') return 'images/playground2.png';
		if (code == 'bike') return 'images/cycle2.png';		
		if (code == 'helmet') return 'images/helmet2.png';
		if (code == 'skateboard') return 'images/skate2.png';	
		if (code == 'pool') return 'images/swim2.png';	
		if (code == 'golf') return 'images/golf2.png';	
		if (code == 'tennis') return 'images/tennis2.png';	
		if (code == 'nursery') return 'images/plantnurse2.png';	
		if (code == 'commgarden') return 'images/garden2.png';	
		if (code == 'pet') return 'images/pet2.png';	
		if (code == 'train') return 'images/train2.png';	
		if (code == 'publicart') return 'images/pubart2.png';	
		if (code == 'wifi') return 'images/wifi2.png';	
		if (code == 'bus') return 'images/bus2.png';	
		if (code == 'ferry') return 'images/ferry2.png';	
		if (code == 'boat') return 'images/boat2.png';	
		if (code == 'star') return 'images/star2.png';	
	}
	

	
	function showmarkers() {
	
		// clear markers first
		/*
		$('#map_canvas').gmap('find', 'markers', { 'tag': 'brizinga' }, function(marker, found) {
			marker.setMap(null);
		});
		*/
		$('#map_canvas').gmap('clear', 'markers', { 'tag' : 'brizinga' });
		
		// put me marker
		var ll = new google.maps.LatLng(this.mylat, this.mylng);
		$('#map_canvas').gmap('get','map').setOptions({'center':ll});
		$('#map_canvas').gmap('addMarker', {
			'tag' : 'me',
			'position': ll, 
			'bounds': false,
			'zoom' : this.zoom,
			'draggable' : true
		}).dragend(function(event) {
			mylat = event.latLng.lat();
			mylng = event.latLng.lng()
			showmarkers();
		});
		/*.show(function() {
			var desc = '<p style="text-align: center; font-family: Arial,sans-serif; font-size: 15px;">You are here</p>';
			$('#map_canvas').gmap('openInfoWindow', { 'content': desc }, this);
		});*/
	
		
		// get new markers near me
		// prod
		var strurl = 'http://www.brizinga.com/data/?mylat=' + this.mylat + '&mylng=' + this.mylng + '&dist=' + dist + 
		// dev
		//var strurl = 'http://localhost/brizinga/www/data/?mylat=' + this.mylat + '&mylng=' + this.mylng + '&dist=' + dist + 
			'&park=' + getpark +
			'&dunny=' + gettoilet +
			'&dogpark=' + getdogpark +
			'&playground=' + getplayground + 
			'&bike=' + getbike + 
			'&skateboard=' + getskateboard + 
			'&pool=' + getpool + 
			'&golf=' + getgolf + 
			'&tennis=' + gettennis + 
			'&commgarden=' + getcommgarden + 
			'&publicart=' + getpublicart +
			'&wifi=' + getwifi + 
			'&helmet=' + gethelmet + 
			'&pet=' + getpet + 
			'&train=' + gettrain+ 
			'&nursery=' + getnursery + 
			'&picnic=' + getpicnic + 
			'&bus=' + getbus +
			'&ferry=' + getferry +
			'&boat=' + getboat +
			'&star=' + getstar;
//			 alert(strurl); // debug
		$.getJSON(strurl, function(data) { 
			$.each( data.markers, function(i, marker) {
				$('#map_canvas').gmap('addMarker', { 
					'tag' : 'brizinga',
					'position': new google.maps.LatLng(marker.lat, marker.lng), 
					'icon' : geticon(marker.code), 
					'title' : marker.name, 
					'animation' : google.maps.Animation.DROP,
					'bounds': false 
				}).click(function() {
					var desc = '';

					if (marker.name != '') {
						desc += '<p style="text-align: left; font-family: Arial,sans-serif; font-size: 15px;">' + marker.name + '</p>';
					}
					if (marker.address != '') {
						desc += '<p style="text-align: left; font-family: Arial,sans-serif; font-size: 12px;">' + marker.address + '</p>';
					}
					if (marker.info != '') {
						desc += '<div style="border-top: #ccc 1px dashed;"></div>'
						desc += '<p style="text-align: left; font-family: Arial,sans-serif; font-size: 10px;"><i>' + marker.info + '</i></p>';
					}
					
					desc += '<a href="#" onclick="gethere(' + marker.lat + ',' + marker.lng + ', \'walk\')" id="gethere"><img src="images/walk1.png"></a>';
					desc += '&nbsp;&nbsp;'
					desc += '<a href="#" onclick="gethere(' + marker.lat + ',' + marker.lng + ', \'drive\')" id="gethere"><img src="images/car1.png"></a>';
					
					$('#map_canvas').gmap('openInfoWindow', { 'content': desc }, this);

				});
			});
		});                                                                                                                                                                                                                      

		// get tweets
		/*
		strurl = 'http://search.twitter.com/search.json?geocode=' + this.mylat + ',' + this.mylng + ',' + twittdist + 'km&callback=?';
		var count = 0;
		while (strurl != '' && count < 10) {
			count ++;
			$.getJSON( strurl, function( data ) {
				$.each( data.results, function( index, item ) {
					if (item.geo != null) {
						$('#map_canvas').gmap('addMarker', { 
							'tag' : 'brizinga',
							'position': new google.maps.LatLng(item.geo.coordinates[0], item.geo.coordinates[1]),
							'icon' : item.profile_image_url, 
							'title' : item.from_user, 
							'bounds': false 
						}).click(function() {
							var desc = '';

							desc += '<p style="text-align: left; font-family: Arial,sans-serif; font-size: 12px;"><a href="http://twitter.com/#!/' + item.from_user + '" target="_blank">' + item.from_user_name + '</a> says:</p>';
							desc += '<p style="text-align: left; font-family: Arial,sans-serif; font-size: 15px;">' + item.text + '</p>';
							
							desc += '<div style="border-top: #ccc 1px dashed;"></div>'
							var date = new Date(Date.parse(item.created_at)).toLocaleString().substr(0, 25);
							
							// item.source = item.source.replace('&lt;', '<').replace('&lt;', '<');
							// item.source = item.source.replace('&quot;', '"').replace('&quot;', '"').replace('&quot;', '"').replace('&quot;', '"');
							// item.source = item.source.replace('\/', '/');
							// item.source = item.source.replace('&gt;', '>').replace('&gt;', '>');
							// item.source = item.source.replace('rel="nofollow"', 'rel="nofollow" target="_blank"');

							desc += '<p style="text-align: left; font-family: Arial,sans-serif; font-size: 10px;"><i>' + date + '</i></p>';

							$('#map_canvas').gmap('openInfoWindow', { 'content': desc }, this);
						});
					}
				});
				if (data.next_page != null) {
					strurl = data.next_page;
				} else {
					strurl = '';
				}
			});
		}
		*/
		
		// refresh map
		$('#mapPage').live("pageshow", function() {
			$('#map_canvas').gmap('refresh');
		});
		
		// center map
		$('#mapPage').live("pageshow", function() {
			$('#map_canvas').gmap({'center':ll});
		});
			
				
	}
	
	
	//put gethere function 
	function gethere(lat, lng, mode){
		if (mode == 'bike') // note aus doesn't have bike becuase we're pov
			$('#map_canvas').gmap('displayDirections', { 'origin': new google.maps.LatLng(this.mylat, this.mylng), 
														 'destination': new google.maps.LatLng(lat, lng), 
														 'travelMode': google.maps.DirectionsTravelMode.BICYCLING }, 
														 { 'panel': document.getElementById('directions')}, function(success, result) {
															// if ( success ) alert('Results found!');
														 });
		if (mode == 'walk')
			$('#map_canvas').gmap('displayDirections', { 'origin': new google.maps.LatLng(this.mylat, this.mylng), 
														 'destination': new google.maps.LatLng(lat, lng), 
														 'travelMode': google.maps.DirectionsTravelMode.WALKING }, 
														 { 'panel': document.getElementById('directions')}, function(success, result) {
															// if ( success ) alert('Results found!');
														 });
		if (mode == 'drive')
			$('#map_canvas').gmap('displayDirections', { 'origin': new google.maps.LatLng(this.mylat, this.mylng), 
														 'destination': new google.maps.LatLng(lat, lng), 
														 'travelMode': google.maps.DirectionsTravelMode.DRIVING }, 
														 { 'panel': document.getElementById('directions')}, function(success, result) {
															// if ( success ) alert('Results found!');
														 });
	}
		
$(document).ready(function(){
	
	
	$('#map_canvas').gmap({'zoom': zoom}).bind('init', function() { 
		$('#map_canvas').gmap('getCurrentPosition', function(position, status) { //use 'getCurrentPosition' for Native, i
			if ( status === 'OK' ) {
				this.mylat = position.coords.latitude;
				this.mylng = position.coords.longitude;
				
				// clear me marker first
				$('#map_canvas').gmap('find', 'markers', { 'tag': 'me' }, function(marker, found) {
					marker.setMap(null);
				});
				//$('#map_canvas').gmap('clear', 'markers', { 'tag' : 'me' });
				
				// put me marker
				var ll = new google.maps.LatLng(this.mylat, this.mylng);
				$('#map_canvas').gmap('get','map').setOptions({'center':ll});
				$('#map_canvas').gmap('addMarker', {
					'tag' : 'me',
					'position': ll, 
					'bounds': false,
					'zoom' : this.zoom
					//,'draggable' : true
				}).show(function() {
					var desc = '<p style="text-align: center; font-family: Arial,sans-serif; font-size: 15px;">You are here</p>';
					
				//	$('#map_canvas').gmap('openInfoWindow', { 
				//		'content': desc 
				//	}, this);
				});	
				
				showmarkers();
				
			}
		});
		
		$('#mapPage').live("pageshow", function() {
			$('#map_canvas').gmap('refresh');
		});
	});
	
	$('#reload').click(function() {
		showmarkers();
	}); 

	
	
	
	//Park
	
	$("#park2").hide(); //hide park icon
	
	$("#park1").click( function(){
		getpark = 'y';
		$("#park1").hide();
		$("#park2").show();
		showmarkers();
	});
	
	$("#park2").click( function(){
		getpark = 'n';
		$("#park2").hide();
		$("#park1").show();
		showmarkers();
	});
	
	//Picnic
	
	$("#picnic2").hide(); //hide park icon
	
	$("#picnic1").click( function(){
		getpicnic = 'y';	
		$("#picnic1").hide();
		$("#picnic2").show();
		showmarkers();
	});
	
	$("#picnic2").click( function(){
		getpicnic = 'n';
		$("#picnic2").hide();
		$("#picnic1").show();
		showmarkers();
	});
	
	//Toilet
	
	$("#toilet2").hide(); //hide park icon
	
	$("#toilet1").click( function(){
		gettoilet = 'y';
		$("#toilet1").hide();
		$("#toilet2").show();
		showmarkers();
	});
	
	$("#toilet2").click( function(){
		gettoilet = 'n';
		$("#toilet2").hide();
		$("#toilet1").show();
		showmarkers();
	});
	
	//Dog
	
	$("#dog2").hide(); //hide park icon
	
	$("#dog1").click( function(){
		getdogpark = 'y';
		$("#dog1").hide();
		$("#dog2").show();
		showmarkers();
	});
	
	$("#dog2").click( function(){
		getdogpark = 'n';
		$("#dog2").hide();
		$("#dog1").show();
		showmarkers();
	});
				
	//Playground
	
	$("#pground2").hide(); //hide park icon
	
	$("#pground1").click( function(){
		getplayground = 'y';
		$("#pground1").hide();
		$("#pground2").show();
		showmarkers();
	});
	
	$("#pground2").click( function(){
		getplayground = 'n';
		$("#pground2").hide();
		$("#pground1").show();
		showmarkers();
	});
						
	//Cycle
	
	$("#cycle2").hide(); //hide park icon
	
	$("#cycle1").click( function(){
		getbike = 'y';
		$("#cycle1").hide();
		$("#cycle2").show();
		showmarkers();
	});
	
	$("#cycle2").click( function(){
		getbike = 'n';
		$("#cycle2").hide();
		$("#cycle1").show();
		showmarkers();
	});
					
	//Helmet
	
	$("#helmet2").hide(); //hide park icon
	
	$("#helmet1").click( function(){
		gethelmet = 'y';
		$("#helmet1").hide();
		$("#helmet2").show();
		showmarkers();
	});
	
	$("#helmet2").click( function(){
		gethelmet = 'n';
		$("#helmet2").hide();
		$("#helmet1").show();
		showmarkers();
	});			
	
	//Skate
	
	$("#skate2").hide(); //hide park icon
	
	$("#skate1").click( function(){
		getskateboard = 'y';
		$("#skate1").hide();
		$("#skate2").show();
		showmarkers();
	});
	
	$("#skate2").click( function(){
		getskateboard = 'n';
		$("#skate2").hide();
		$("#skate1").show();
		showmarkers();
	});			
	
	//Swim
	
	$("#swim2").hide(); //hide park icon
	
	$("#swim1").click( function(){
		getpool = 'y';
		$("#swim1").hide();
		$("#swim2").show();
		showmarkers();
	});
	
	$("#swim2").click( function(){
		getpool = 'n';
		$("#swim2").hide();
		$("#swim1").show();
		showmarkers();
	});			
	
	//Golf
	
	$("#golf2").hide(); //hide park icon
	
	$("#golf1").click( function(){
		getgolf = 'y';
		$("#golf1").hide();
		$("#golf2").show();
		showmarkers();
	});
	
	$("#golf2").click( function(){
		getgolf = 'n';
		$("#golf2").hide();
		$("#golf1").show();
		showmarkers();
	});		

	//Tennis
	
	$("#tennis2").hide(); //hide park icon
	
	$("#tennis1").click( function(){
		gettennis = 'y';
		$("#tennis1").hide();
		$("#tennis2").show();
		showmarkers();
	});
	
	$("#tennis2").click( function(){
		gettennis = 'n';
		$("#tennis2").hide();
		$("#tennis1").show();
		showmarkers();
	});				
				

	//Plant
	
	$("#plant2").hide(); //hide park icon
	
	$("#plant1").click( function(){
		getnursery = 'y';
		$("#plant1").hide();
		$("#plant2").show();
		showmarkers();
	});
	
	$("#plant2").click( function(){
		getnursery = 'n';
		$("#plant2").hide();
		$("#plant1").show();
		showmarkers();
	});				
		
		
	//Garden
	
	$("#garden2").hide(); //hide park icon
	
	$("#garden1").click( function(){
		getcommgarden = 'y';
		$("#garden1").hide();
		$("#garden2").show();
		showmarkers();
	});
	
	$("#garden2").click( function(){
		getcommgarden = 'n';
		$("#garden2").hide();
		$("#garden1").show();
		showmarkers();
	});	
	
	//Pet
	
	$("#pet2").hide(); //hide park icon
	
	$("#pet1").click( function(){
		getpet = 'y';
		$("#pet1").hide();
		$("#pet2").show();
		showmarkers();

	});
	
	$("#pet2").click( function(){
		getpet = 'n';
		$("#pet2").hide();
		$("#pet1").show();
		showmarkers();

	});				
	
	//Public Art
	
	$("#pubart2").hide(); //hide park icon
	
	$("#pubart1").click( function(){
		getpublicart = 'y';
		$("#pubart1").hide();
		$("#pubart2").show();
		showmarkers();
	});
	
	$("#pubart2").click( function(){
		getpublicart = 'n';
		$("#pubart2").hide();
		$("#pubart1").show();
		showmarkers();
	});				
										
	//Wifi
	
	$("#wifi2").hide(); //hide park icon
	
	$("#wifi1").click( function(){
		getwifi = 'y';
		$("#wifi1").hide();
		$("#wifi2").show();
		showmarkers();
	});
	
	$("#wifi2").click( function(){
		getwifi = 'n';
		$("#wifi2").hide();
		$("#wifi1").show();
		showmarkers();
	});				
	
	//Bus
	
	$("#bus2").hide(); //hide park icon
	
	$("#bus1").click( function(){
		getbus = 'y';
		$("#bus1").hide();
		$("#bus2").show();
		showmarkers();
	});
	
	$("#bus2").click( function(){
		getbus = 'n';
		$("#bus2").hide();
		$("#bus1").show();
		showmarkers();
	});				
	
	//Train
	
	$("#train2").hide(); //hide park icon
	
	$("#train1").click( function(){
		gettrain = 'y';
		$("#train1").hide();
		$("#train2").show();
		showmarkers();
	});
	
	$("#train2").click( function(){
		gettrain = 'n';
		$("#train2").hide();
		$("#train1").show();
		showmarkers();
	});				
	
	//Ferry
	
	$("#ferry2").hide(); //hide park icon
	
	$("#ferry1").click( function(){
		getferry = 'y';
		$("#ferry1").hide();
		$("#ferry2").show();
		showmarkers();
	});
	
	$("#ferry2").click( function(){
		getferry = 'n';
		$("#ferry2").hide();
		$("#ferry1").show();
		showmarkers();
	});				
	
	
	//Boat
	
	$("#boat2").hide(); //hide park icon
	
	$("#boat1").click( function(){
		getboat = 'y';
		$("#boat1").hide();
		$("#boat2").show();
		showmarkers();
	});
	
	$("#boat2").click( function(){
		getboat = 'n';
		$("#boat2").hide();
		$("#boat1").show();
		showmarkers();
	});				
	
	
	//Star
	
	$("#star2").hide(); //hide park icon
	
	$("#star1").click( function(){
		getstar = 'y';
		$("#star1").hide();
		$("#star2").show();
		showmarkers();
	});
	
	$("#star2").click( function(){
		getstar = 'n';
		$("#star2").hide();
		$("#star1").show();
		showmarkers();
	});				


	
});