<?php
/*
 * Copyright (c) 2012 Steve Shearn. All rights reserved.
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 *  
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *  
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */



// database connection parameters
include('cfgprod.php');


// Using Haversine formula to calulate distance by radius: http://en.wikipedia.org/wiki/Haversine_formula
// Using example from this tutorial: https://developers.google.com/maps/articles/phpsqlsearch_v3

$mylat = $_GET['mylat'];
$mylong = $_GET['mylng'];
$dist = $_GET['dist'];
if ($mylat == '') die('Need your location.');
if ($mylong == '') die('Need your location.');
if ($dist == '') die('Need distance from you.');

if (!is_numeric($mylat)) die('Need your location.');
if (!is_numeric($mylong)) die('Need your location.');
if (!is_numeric($dist)) die('Need distance from you.');

// use km/miles
define("USE_KMS", true);
$haversin = 6371; if (!USE_KMS) $haversin = 3959;

// query defaults to get nothing
$qry = 'SELECT 
          code,
          latitude,
          longitude,
          ( ' . $haversin . ' * acos( cos( radians(' . $mylat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $mylong . ') ) + sin( radians(' . $mylat . ') ) * sin( radians( latitude ) ) ) ) AS distance,
          name,
          address,
          info
        FROM
          places
        HAVING distance < ' . $dist . '
        AND ( code = ""
        ';

// build query parameters
if ($_GET['tennis'] == 'y') $qry .= ' OR code = "tennis"';
if ($_GET['park'] == 'y') $qry .= ' OR code = "park"';
if ($_GET['skateboard'] == 'y') $qry .= ' OR code = "skateboard"';
if ($_GET['commgarden'] == 'y') $qry .= ' OR code = "commgarden"';
if ($_GET['pool'] == 'y') $qry .= ' OR code = "pool"';
if ($_GET['golf'] == 'y') $qry .= ' OR code = "golf"';
if ($_GET['playground'] == 'y') $qry .= ' OR code = "playground"';
if ($_GET['dunny'] == 'y') $qry .= ' OR code = "dunny"';
if ($_GET['dogpark'] == 'y') $qry .= ' OR code = "dogpark"';
if ($_GET['publicart'] == 'y') $qry .= ' OR code = "publicart"';
if ($_GET['bike'] == 'y') $qry .= ' OR code = "bike"';
if ($_GET['wifi'] == 'y') $qry .= ' OR code = "wifi"';
if ($_GET['helmet'] == 'y') $qry .= ' OR code = "helmet"';
if ($_GET['pet'] == 'y') $qry .= ' OR code = "pet"';
if ($_GET['nursery'] == 'y') $qry .= ' OR code = "nursery"';
if ($_GET['picnic'] == 'y') $qry .= ' OR code = "picnic"';
if ($_GET['bus'] == 'y') $qry .= ' OR code = "bus"';
if ($_GET['boat'] == 'y') $qry .= ' OR code = "boat"';
if ($_GET['ferry'] == 'y') $qry .= ' OR code = "ferry"';
if ($_GET['star'] == 'y') $qry .= ' OR code = "star"';
if ($_GET['train'] == 'y') $qry .= ' OR code = "train"';

$qry .= ') ORDER BY distance'; // LIMIT 0 , 10';   *** do limits later

// setup connection
$conn = mysql_connect(CFG_DB_HOST, CFG_DB_USER, CFG_DB_PASSWORD);
if (!$conn) {
  die('Database connection error. Sorry.');
}

if (!mysql_select_db(CFG_DB_NAME, $conn)) {
  mysql_close($conn);
  die('Database selection error. Sorry.');
}

// exec query
$result = mysql_query($qry);
if (!$result) {
  mysql_close($conn);
  die('Database execution error. Sorry.');
}

// JSON out
while ($row = mysql_fetch_assoc($result)) {
$rowdata[] = array('code' => $row['code'],
                   'lat' => $row['latitude'],
                   'lng' => $row['longitude'],
                   'dist' => $row['distance'],
                   'name' => $row['name'],
                   'address' => $row['address'],
                   'info' => $row['info']);
}

mysql_free_result($result);

echo json_encode(array('markers' => $rowdata));

?>